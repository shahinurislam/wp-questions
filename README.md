1. add_filter() and apply_filters() – WordPress Filter Hooks
	Ans:
	
	Often we come across instances where we have to make customization in WordPress Theme’s or Plugins. WordPress offers us filter hooks to add or modify the code just as per our requirement.
There are two types of Hooks: Actions and Filters. Actions are used to add some data while Filters are used to modify existing data.

Here in this post we will be guiding How and When to use Filters. To know about Actions read out add_action() and do_action() – WordPress Actions

In layman language, WP hooks are just like door-hooks. Our Plugin or theme is the door. We use actions to hang clothes (not already there) on to the door and use filters to add clothes over already hung ones.

For example, we have a function that greets employees daily:


//to Greet employees
function greet_employee() {
   $greetings = "Good Morning!!";
   return $greetings;
}

//to Greet employees
function greet_employee() {
   $greetings = "Good Morning!!";
   return $greetings;
}
Our above function cannot be modified, and to make our code customizable we could use:


//to Greet employees
function greet_employee() {
   $greetings = "Good Morning!!";
   $greetings = apply_filters( 'greet_employee_filter', $greetings );
   return $greetings;
}

//to Greet employees
function greet_employee() {
   $greetings = "Good Morning!!";
   $greetings = apply_filters( 'greet_employee_filter', $greetings );
   return $greetings;
}
greet_employee_filter is a filter hook that can be used to modify the content, $greetings being the default value to be returned.

Well, one of the employees has his birthday today and we wish to congratulate him, we definitely cannot use the above function. Here comes the savior!! add_filter()
We could use:


add_filter( 'greet_employee_filter','employee_birthday' );

add_filter( 'greet_employee_filter','employee_birthday' );
We have attached a callback function to the filter hook. Let’s define the function:


//to greet employees on birthday
function employee_birthday() {
   $greetings = "Good Morning and wish you a very Happy Birthday!!";
   return $greetings;
}

//to greet employees on birthday
function employee_birthday() {
   $greetings = "Good Morning and wish you a very Happy Birthday!!";
   return $greetings;
}
Similarly, we can attach as many callbacks to the filter hook as we may desire.
We can set priorities of callbacks, one with lower number will execute first (by default functions with the same priority are executed in the order in which they were added to the action). Suppose we have two callbacks


employee_birthday()

employee_birthday()
and


employee_weddding_anniversary()

employee_weddding_anniversary()
and we want to call the second one first, we can do it like this:


add_filter( 'greet_employee_filter','greet_employee_birthday',99 );
add_filter( 'greet_employee_filter','employee_weddding_anniversary',100 )

add_filter( 'greet_employee_filter','greet_employee_birthday',99 );
add_filter( 'greet_employee_filter','employee_weddding_anniversary',100 )
If you are customizing a plugin or theme look for apply_filters() and attach your custom callback function to the filter hook at the appropriate place using add_filter().
If you are creating a plugin or theme, add apply_filters() in your code to make it customizable in the future by you and others.

link: http://wpdevstudio.com/add_filter-and-apply_filter-wordpress-filter-hooks/


Other example: 

Here’s some more about Filter hooks and the stuff you can do with ’em:

Let’s take example of function greet_employee().


//to greet employees
function greet_employee() {
   $greetings = "Good Morning!!";
   $greetings = apply_filters( 'greet_employee_filter', $greetings );
   return $greetings;
}

//to greet employees
function greet_employee() {
   $greetings = "Good Morning!!";
   $greetings = apply_filters( 'greet_employee_filter', $greetings );
   return $greetings;
}

add_action( 'greet_employee_filter','employee_birthday' ); //callback attached with filter hook

add_action( 'greet_employee_filter','employee_birthday' ); //callback attached with filter hook

//filter hook callback to greet on other occasion 
function employee_birthday() {
   $greetings = "Good Morning and wish you a very Happy Birthday!!";
   return $greetings;
}

//filter hook callback to greet on other occasion 
function employee_birthday() {
   $greetings = "Good Morning and wish you a very Happy Birthday!!";
   return $greetings;
}
We want to greet employees personally. We can pass Employee’s name as parameter to the add_action(). See WordPress Codex.

So our function would be:


//to greet employees using names function greet_employee() {
   $greetings = "Good Morning!!";
   $greetings = apply_filters( 'greet_employee_filter', $greetings, $name );
   return $greetings;
}

//to greet employees using names function greet_employee() {
   $greetings = "Good Morning!!";
   $greetings = apply_filters( 'greet_employee_filter', $greetings, $name );
   return $greetings;
}
Attaching callback with the filter hook:


add_filter( 'greet_employee_filter','employee_birthday', 10, 2); //10 is the priority of callback function and 2 denotes the number of arguments passed (one being the default value)

add_filter( 'greet_employee_filter','employee_birthday', 10, 2); //10 is the priority of callback function and 2 denotes the number of arguments passed (one being the default value)
And callback function can be defined like this:


//filter hook callback to greet on other occasion using name 
function employee_birthday($name) {
   $greetings = "Good Morning and wish you a very Happy Birthday ".$name. "!!";
   return $greetings;
}

//filter hook callback to greet on other occasion using name 
function employee_birthday($name) {
   $greetings = "Good Morning and wish you a very Happy Birthday ".$name. "!!";
   return $greetings;
}
Note that callback associated with filter hook ALWAYS returns some value.

WordPress has several filter hooks that can be used to modify the data. For eg.

the_content – for modification of the content of posts,
the_title – for modification of the title of posts,
the_excerpt – for modification of the excerpt
You can refer to Filter Reference in WordPress Codex for more hooks to make customization!

You can also remove the default callback attached to any specific filter hook using remove_filter() function.

For eg you want to remove default callback to the the_excerpt filter hook (and probably replace it with your custom function), here it goes:


remove_filter( 'the_excerpt', 'the_excerpt' );

remove_filter( 'the_excerpt', 'the_excerpt' );
where first argument the_excerpt is filter hook and second is the callback function attached the_excerpt(), an optional parameter being the priority of the callback.

link : http://wpdevstudio.com/add_filter-and-apply_filters-more-on-wordpress-filter-hooks/
 https://wordpress.stackexchange.com/questions/99630/how-to-return-hook-data-when-multiple-parameters-are-present
 https://codex.wordpress.org/Function_Reference/apply_filters_ref_array
 https://wordpress.stackexchange.com/questions/233071/apply-filters-with-multiple-args-and-multiple-add-filter
 https://wordpress.stackexchange.com/questions/39488/how-to-add-filter-with-2-args
 

Video : https://www.youtube.com/watch?v=SmaMFF57ixY




/*=======================Customizer header image================================*/
https://codex.wordpress.org/Custom_Headers
https://developer.wordpress.org/themes/functionality/custom-headers/

/====================================Customizer header text==================================/
<?php
add_action( 'customize_register', 'genesischild_register_theme_customizer' );
/*
 * Register Our Customizer Stuff Here
 */
function genesischild_register_theme_customizer( $wp_customize ) {
	// Create custom panel.
	$wp_customize->add_panel( 'text_blocks', array(
		'priority'       => 500,
		'theme_supports' => '',
		'title'          => __( 'Text Blocks', 'genesischild' ),
		'description'    => __( 'Set editable text for certain content.', 'genesischild' ),
	) );
	// Add Footer Text
	// Add section.
	$wp_customize->add_section( 'custom_footer_text' , array(
		'title'    => __('Change Footer Text','genesischild'),
		'panel'    => 'text_blocks',
		'priority' => 10
	) );
	// Add setting
	$wp_customize->add_setting( 'footer_text_block', array(
		 'default'           => __( 'default text', 'genesischild' ),
		 'sanitize_callback' => 'sanitize_text'
	) );
	// Add control
	$wp_customize->add_control( new WP_Customize_Control(
	    $wp_customize,
		'custom_footer_text',
		    array(
		        'label'    => __( 'Footer Text', 'genesischild' ),
		        'section'  => 'custom_footer_text',
		        'settings' => 'footer_text_block',
		        'type'     => 'text'
		    )
	    )
	);
 	// Sanitize text
	function sanitize_text( $text ) {
	    return sanitize_text_field( $text );
	}
}



-----------front end--------------
<?php

// Change the footer text in Genesis with a back up if blank
add_filter('genesis_footer_creds_text', 'genesischild_footer_text');

function genesischild_footer_text() {
	if( get_theme_mod( 'footer_text_block') != "" ) {
		echo get_theme_mod( 'footer_text_block');
	}
	else{
		echo 'Copyright &copy; 2016 · Genesis Sample Theme on Genesis Framework · WordPress · Log out'; // Add you default footer text here
	}

}

// Regular WordPress theme just add in the footer template

   <?php if( get_theme_mod( 'footer_text_block') != "" ): ?>
            <p class="footer-text">
                <?php echo get_theme_mod( 'footer_text_block'); ?>
            </p>
			
--------------------------------------------------------------------------------








